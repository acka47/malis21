- DONE Aufgaben fertigmachen
  id:: 61883d4b-a36f-48d1-94bf-dad25f2e5b11
- DONE JSON-Schema-Teil ergänzen
- DONE Mermaid-Diagramm ergänzen (evtl. in der Aufgabe, siehe ((61883d4b-a36f-48d1-94bf-dad25f2e5b11)))
  :LOGBOOK:
  CLOCK: [2021-11-07 Sun 21:56]
  :END:
- DONE Nachricht an Student:innen rausschicken
  :LOGBOOK:
  CLOCK: [2021-11-08 Mon 21:40:52]--[2021-11-10 Wed 19:12:49] =>  45:31:57
  :END:
- DONE Slides machen
  :LOGBOOK:
  CLOCK: [2021-11-10 Wed 20:52:27]--[2021-11-13 Sat 10:03:10] =>  61:10:43
  :END:
- DONE Fragen, ob Aufzeichnung ok ist -> aufzeichnen
- DONE ((61d559ac-7634-4d41-be44-9211f47ef1b7))-Beispiel ergänzen
- DONE [[SPARQL]]-Dinge ergänzen
- DONE SkoHub-Vocabs-Dokumentation anlegen -> https://github.com/skohub-io/skohub.io/issues/19
  :LOGBOOK:
  CLOCK: [2022-01-05 Wed 10:04:17]--[2022-01-22 Sat 17:49:19] =>  415:45:02
  :END:
- DONE [[Suchmaschinentechnologie]] fertigstellen
  :LOGBOOK:
  CLOCK: [2022-01-05 Wed 10:01:22]--[2022-01-22 Sat 23:30:55] =>  421:29:33
  :END:
- DONE Folien erstellen
  :LOGBOOK:
  CLOCK: [2022-01-22 Sat 17:49:27]--[2022-01-22 Sat 23:30:57] =>  05:41:30
  :END:
- DONE [[Aufgabe 4: Erstellung und Publikation eines SKOS-Vokabulars]] fertigstellen
  :LOGBOOK:
  CLOCK: [2022-01-05 Wed 10:02:24]
  CLOCK: [2022-01-05 Wed 10:03:45]--[2022-01-22 Sat 23:31:00] =>  421:27:15
  :END:
- LATER interne slide-Links ergänzen
-
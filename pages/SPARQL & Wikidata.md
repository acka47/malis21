title:: SPARQL & Wikidata

- ## SPARQL Protocol and RDF Query Language
  title:: SPARQL & Wikidata
  * Sprache/Protokoll zur Abfrage und Modifikation von RDF-Graphen
  * Grundidee: Beschreibung von Subgraphen, die in dem abzufragenden Graphen enthalten sind inklusive`?Variabeln`
  * Server antwortet mit passenden Knoten/URIs, für die das Graphmuster passt
- ## Gut zu SPARQLn: Wikidata
  * ein Knowledge Graph
  * ein Wikimedia-Projekt
  * für Menschen und Maschinen
  * multilingual
  * kollaborativ gepflegt & offen für alle
  * CC0
  * basiert auf der freien Software [Wikibase](https://wikiba.se/)
- ## Wikidata-Datenmodell
  ![](../assets/wikidata-datenmodell.png)
  <small>Quelle: <a href="https://commons.wikimedia.org/wiki/File:Datamodel_in_Wikidata_de.svg">Wikimedia Commons</a>, Lizenz: <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.de">CC0</a></small>
- ## Beispiel Wikidata-Query I
  ```sparql
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  
  SELECT ?item ?itemLabel
  WHERE
  {
    ?item ?p ?o;
      rdfs:label ?itemLabel .
  } LIMIT 10
  ```
- ## Beispiel Wikidata-Query II
  ```sparql
  SELECT ?item ?itemLabel
  WHERE
  {
    ?item wdt:P31 wd:Q5; # is human
          wdt:P512 wd:Q6785216 ; # holds an MLIS
          rdfs:label ?itemLabel .
    FILTER (langmatches(lang(?itemLabel), "en")  
            && !langmatches(lang(?itemLabel), "en-ca") 
            && !langmatches(lang(?itemLabel), "en-gb"))
  }
  ```
  Direktlink: [https://w.wiki/4hFu](https://w.wiki/4hFu)
- ## Beispiel Wikidata-Query III
  ```sparql
  SELECT ?item ?itemLabel
  WHERE
  {
    ?item wdt:P31 wd:Q5; # is human
          wdt:P512 wd:Q6785216 . # holds an MLIS
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],de". } 
            # Wikibase-spezifischer, nicht SPARQL-konformer Label-Dienst
  }
  ```
  Direktlink: [https://w.wiki/4hFv](https://w.wiki/4hFv)
- ## Beispiel Wikidata-Query IV
  ```sparql
  #defaultView:ImageGrid
  
  SELECT ?educationalInstitution ?image
  WHERE
  {
    ?item wdt:P31 wd:Q5; # is human
          wdt:P512 wd:Q6785216 ; # holds an MLIS
          wdt:P69 ?educationalInstitution .
    ?educationalInstitution wdt:P18 ?image .
  }
  ```
  Direktlink: [https://w.wiki/4hG9](https://w.wiki/4hG9)
- ## Beispiel Wikidata-Query V
  ```sparql
  #defaultView:Map
  
  SELECT ?educationalInstitution ?geo
  WHERE
  {
    ?item wdt:P31 wd:Q5; # is human
          wdt:P512 wd:Q6785216 ; # holds an MLIS
          wdt:P69 ?educationalInstitution .
    ?educationalInstitution wdt:P625 ?geo .
  }
  ```
  Direktlink: [https://w.wiki/4hGA](https://w.wiki/4hGA)
- ## Übung: ((61ec677c-d398-45e1-96b4-16e83f25767c))
- ## Interessante Dinge auf Wikidata-Basis
- ## Scholia
  [Scholia](https://scholia.toolforge.org/): ein Projekt zur Sammlung bibliographischer Informationen und von Personen- und Institutionsprofilen
  ![](../assets/scholia-screenshot-foerstner.png)
  Quelle: [Scholia-Profil von Konrad Förstner](https://scholia.toolforge.org/author/Q18744528)
- ## Die Datenlaube
  * Citizen-Science-Projekt zur Erschließung von Artikeln (1853 bis 1899) der deutschsprachigen Illustrierten "Die Gartenlaube"
  * Ein großes vernetztes Projekt, in Wikisource, Wikimedia Commons und Wikidata, begründet von zwei Bibliothekaren und mit ersten Nutzungen in den Digital Humanities.
  * Zum Einstieg empfiehlt sich [der erste Blogbeitrag](https://diedatenlaube.github.io/die_datenlaube_der_gartenlaube) und der [Zwischenbericht des „SFB 1853“ – zwei Jahre #DieDatenlaube](https://saxorum.hypotheses.org/5692)
- ## NWBib-Raumsystematik
  ![](../assets/nwbib-spatial.png)
  Quelle: https://nwbib.de/spatial, siehe auch [Code4Lib-Artikel zum Thema](https://blog.lobid.org/2021/06/16/nwbib-wikidata-code4lib.html)
- ## Sucherweiterung der ETH Zürich
  ![Slide 9  der Swisscovery-SWIB20-Präsentation mit der Abbildung einer Personenvorschlagsliste basierend auf dem Suchbegriff](../assets/eth.png){:height 445, :width 776}
  Quelle: [SWIB20-Präsentation von Bernd Uttenweiler](https://swib.org/swib21/programme.html#abs102)
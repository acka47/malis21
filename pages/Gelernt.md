- ## Vorbereitung
	- Bei neuen Themen spätestens zwei Monate vor der Lehrveranstaltung mit der Vorbereitung beginnen
	- Zu viel Material für Präsenzphase 1 & 2, bei 3 hätte es auch ein wenig mehr sein können
	- logseq hat sich gut zur Einarbeitung in neue Themen geeignet als auch noch keine klare lineare Struktur klar war. Für die dritte Präsenzphase, bei der ich aufgrund vorheriger Tutorials/Workshops und bereits vorhandenem Materials schnell eine fertige Struktur hatte, wäre wahrscheinlich auch das direkte Erstellen der Slides sinnvoll gewesen. Auch wegen ((6200cafb-14e6-47ef-8ca0-91fc5c9423c2))
	- Ich habe recht viel Arbeit in mein Setup gesteckt (logseq, Slides, Domain, Static Sides), was sich aber gelohnt hat.
- ## Präsenzphasen
	- Präsentationsfunktion mit reveal.js in logseq ist buggy und regelmäßiges Wechseln in den Browser inkl. Umschalten des geteilten Fensters lenkt ab/verwirrt
	- Regelmäßige praktische Übungen zwischendurch – auch mal nur 5 Minuten – sind wichtig und waren eigentlich auch gut verteilt. Beste Erfahrung war in der dritten Präsenzphase, bei den ersten beiden gab es ja Probleme mit dem Tooling (curl, jq)
	- Gemeinsames HedgeDoc-Pad wurde nicht viel genutzt und ich habe es in der 2. und 3. Phase nicht mehr angeboten
- ## Technik
	- größtes Problem: fehlende Kommandozeile (curl, jq) bei den Studierenden & meist schwieriger Installationsprozess
	- Online-Tools waren meist kein adäquater Ersatz (insbesondere https://reqbin.com/curl war das verkehrte Tool, weil es Redirects folgt und dazu aber nichts ausgibt)
	- STRG+Mauslklick zum Öffnen eines Links in einem neuen Fenster führt in reveal-md-Folien zum Reinzoomen, was extrem nervig ist
	  id:: 61fae7b5-a909-4b15-966b-e1a8a4fbe674
	- Der Zoom-Linux-Client war blöd, weil er die Kameras der Teilnehmer\*innen immer nur in einem kleinen Fenster gezeigt hat, wenn ich ein Fenster geteilt habe – obwohl ich ab der 2. Präsenzveranstaltung zwei Monitore hatte
	- Moodle: naja. Ein vielseitiges Tool mit klobigem UI, von dem wir nur wenige Funktionen genutzt haben. Wichtig war das Forum, den Rest hatte ich auch anderswo (im Web) abgedeckt:
	  * Statt Abgabe: E-Mail
	  * Bewertung wird im MALIS eh nicht in Moodle genutzt
	  * Dateiupload mache ich direkt auf einen Webserver etc.
	- Zur Publikation der Materialien als OER im Web:
		- Transformation von logseq in reveal.js-Slides war zu aufwändig, weil viel händisch gemacht. Sollte ich mehr automatisieren.
		  id:: 6200cafb-14e6-47ef-8ca0-91fc5c9423c2
		- Eventuell ist logseq beim nächsten Mal auch gar nicht nötig. Die Studierenden schauen sich glaube ich eh lieber die Slides an und ich werde nicht mehr von Grund auf alles neu entwickeln müssen (wofür logseq gut ist). So könnte es sinnvoll sein, nächstes Mal einfach direkt an den Slides arbeiten
		- Publikation mit Codeberg Pages hat gut funktioniert.
		- Ich habe anfangs viel gelernt (git, bash) mit verschiedenen Codeberg-GitHub-Pages-Lösungsansätzen.
- ## Aufgaben
	- [Aufgabe 1]([https://pad.gwdg.de/malis21-it2-aufgaben1](https://pad.gwdg.de/s/malis21-it2-aufgaben1#)) kann weiter so gemacht werden: Wahl zwischen technischer Aufgabe und Markup-Aufgabe ist super
	- Für die [JSON-Schema Aufgabe (1.1) ](https://codeberg.org/acka47/malis21-aufgabe1.1) sollten beim nächsten Mal noch die Test-Dateien ein bisschen getweakt werden, siehe https://codeberg.org/acka47/malis21-aufgabe1.1/issues/2
	- Für die [Markdown-Aufgabe (1.2)](https://pad.gwdg.de/s/malis21-aufgabe1.2) ein Link zu einem Style-Guide mitgeben, z.B. https://google.github.io/styleguide/docguide/style.html#document-layout ist sinnvoll. Besser etwas auf Deutsch (evtl. selbst schreiben?).
	- [[Aufgabe 4: Erstellung und Publikation eines SKOS-Vokabulars]] : mal schauen, ob das jemand macht & wie es ankommt
	- Verschiedene Abgabemöglichkeiten (Moodle oder E-Mail) sind nötig (weil kein Dokumentupload nötig), bedeuten ein wenig mehr Aufwand beim Tracken der Abgaben, was aber gut funktioniert hat
	- direktes Durchsehen und zügige Rückmeldung kurz nach Abgabe kam gut an und ist sinnvoll, wenn es sich einrichten lässt (sowohl für mich als auch für die Studierenden)
	- Eventuell die Aufgaben konsistent benennen. In Moodle gibt es eindeutige IDs für alle Aufgaben des Studium (z.B. Aufgabe 4 = MALIS 21.2 IT2.4).
- ## Zusammenarbeit an der TH
	- sehr nette Kolleg\*innen, die mir keinen Stress machen und schnelle gute Rückmeldungen geben
	- nach Rückmeldungen/Wünschen von Studierenden schnell mit anderen Dozent\*innen von IT1 etc. ausgetauscht und mögliche Lösungen besprochen (siehe ((61fae7e9-274f-491a-ba79-7addeaeb0fcb)))
- ## Verbesserungsideen
	- Als Hauptdesiderat aus der Evaluation (5 erfasste Fragebögen) ergeben sich folgende drei Punkte (in Bezug auf das ganze Semester und jede einzelne Präsenzphase):
		- Die Lernziele klar umreißen
		- Die Bedeutung der Lerninhalte für den Studiengang und die praktische Arbeit im Bibliothekskontext klar herausarbeiten.
		- Die Struktur der Lerninhalte nachvollziehbar darstellen.
	- Studierende sollten am besten vorher eine wenig CLI- und git-Erfahrungen gesammelt haben
	  id:: 61fae7e9-274f-491a-ba79-7addeaeb0fcb
	- Die Einführung in die Grundlagen (Internet/Web, URIs, HTTP, HTML) war zu theoretisch und sollte mehr und von Anfang an mit kleinen Übungen verbunden sein (am besten im Browser starten, den ja alle täglich benutzen, dann Developer Tools, auch für HTTP-Kram)
	- Problem lösen: ((61fae7b5-a909-4b15-966b-e1a8a4fbe674))
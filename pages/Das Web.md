## The Web, aka World Wide Web, WWW
## "vague but exciting"
![proposal.png](../assets/proposal_1633461830338_0.png) 
Quelle: [http://info.cern.ch/Proposal.html](http://info.cern.ch/Proposal.html)
- ## Die Grundbausteine des WWW
  * basiert ursprünglich auf [[URIs]], [[HTTP]], [[HTML]]
  * mit der Zeit kamen dazu: [[CSS]], [[JavaScript]]
- Auf den Schultern des [Internet]([[Netzwerkprotokolle]])
  ![http-layers.png](../assets/http-layers_1633461941849_0.png){:width 700}
  <small>Quelle: <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview/http-layers.png">Mozilla Contributors</a>, Lizenz: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a></small>
-
## <img src="https://www.w3.org/Icons/SW/Buttons/sw-skos-magenta-v.svg" width=300 style="border: none; box-shadow: none;" /> 
* SKOS: **S**imple **K**nowledge **O**rganization **S**ystem
* Datenmodell für die maschinenlesbare und webbasierte Publikation kontrollierter Vokabulare als Linked Open Data2009 als [Empfehlung des World Wide Web Consortiums (W3C)](https://www.w3.org/TR/2009/REC-skos-reference-20090818/) veröffentlicht
* Unterstützt Mehrsprachigkeit, Abbildung von Beziehungen & einiges mehr
- ## Kontrollierte Vokabulare als LOD
  * **auflösbare IDs**: ein HTTP URI (Uniform Resource Identifier) für ein Vokabular und jeden Wert
  * **Verlinkung**: Werte sind innerhalb eines Vokabulars und zwischen Vokabularen verlinkt
  * **Erweiterbarkeit**: weitere Aussagen oder Funktionen auf Basis anderer Webstandards können ergänzt werden
- ## Vorteile
  * Webintegration
  * Maschinenlesbarkeit
  * Interoperabilität
  * Nachnutzbarkeit
  * Entkoppelung von Datenmodellierung und Softwareentwicklung
- ## SKOS-Namespace und -Prefix
  `@prefix skos: <http://www.w3.org/2004/02/skos/core#>.`
- ## Aufbau eines SKOS-Vokabulars
  Zwei grundlegende Typen von Einträgen:
  * `ConceptScheme`: 
    * das Vokabular selbst, wird genau **1 Mal** pro Vokabular definiert
    * generelle Informationen wie Lizenz, Titel
  * `Concept`:
    * ein Wert des Vokabulars
    * Informationen wie Label, Beziehungen
- ## Bsp.: Concept Scheme
  ```
  @prefix colour: <https://example.org/colour-with-hierarchy/> .
  @prefix dct: <http://purl.org/dc/terms/> .
  @prefix skos: <http://www.w3.org/2004/02/skos/core#> .
  
  colour: a skos:ConceptScheme ;
      dct:title "Colour Vocabulary"@en, "Farbvokabular"@de ;
      dct:creator "Hans Dampf"@de ;
      dct:created "2021-11-02" ;
      dct:license <https://creativecommons.org/publicdomain/zero/1.0/> .
  ```
- ## Wichtige SKOS-Properties
  * Verbindung zwischen Vokabular und seinen Werten: `hasTopConcept`, `topConceptOf`, `inScheme`
  * Vorzugsbezeichnung: `prefLabel` 
  * Alternativbezeichnung: `altLabel`
  * hierarchische Informationen: `narrower`/`broader`
- ## Bsp.: SKOS Concept I
  ```
  colour:violet a skos:Concept ;
      skos:prefLabel "Violett"@de, "violet"@en ;
      skos:altLabel "Lila"@de, "purple"@en ;
      skos:topConceptOf colour: .
  
  colour: skos:hasTopConcept colour:violet .
  ```
- ## Bsp.: SKOS Concept II
  ```
  colour:green a skos:Concept ;
      skos:prefLabel "Green"@en, "Grün"@de ;
      skos:narrower colour:grasgreen ;
      skos:topConceptOf colour: .
      
  colour:grassgreen a skos:Concept ;
      skos:prefLabel "grass-green"@en, "Grasgrün"@de ;
      skos:inScheme colour: .
  ```
- ## Sonstige Properties I
  * Notation: `notation`
  * Dokumentation des Vokabulars und seiner Entwicklung: `changeNote`, `definition`, `editorialNote`, `example`, `note`, `historyNote`, `scopeNote`
  * Externe Relationen: `broadMatch`, `narrowMatch`, `exactMatch`, `closeMatch`, `mappingRelation`
- ## Bsp.: SKOS Concept III
  ```
  @prefix skos: <http://www.w3.org/2004/02/skos/core#> .
  
  <https://psyndex.de/vocab/psycharchives-types/conferenceObject>
      a skos:Concept ;
      skos:altLabel "Konferenzveröffentlichung"@de, "conference output"@en ;
      skos:exactMatch <http://purl.org/coar/resource_type/c_c94f> ;
      skos:example "presentation slides, conference programs and abstract collections, posters"@en ;
      skos:inScheme <https://psyndex.de/vocab/psycharchives-types/> ;
      skos:narrowMatch <http://purl.org/spar/fabio/ConferencePoster>, <http://purl.org/spar/fabio/Presentation> ;
      skos:notation "7" ;
      skos:prefLabel "Conference Object"@en, "Konferenzobjekt"@de ;
      skos:scopeNote "conference-related publications"@en ;
      skos:topConceptOf <https://psyndex.de/vocab/psycharchives-types/> .
  ```
  Quelle: http://zpidvokabulare.surge.sh/psyndex.de/vocab/psycharchives-types/conferenceObject.de.html
- ## Sonstige Properties II
  * `Collection`, `OrderedCollection`, `member`, `memberList`
  * `hiddenLabel`
  * `broaderTransitive`, `narrowerTransitive`, `related`, `semanticRelation`
  * Siehe auch diese [erläuterte Liste der Elemente](https://dini-ag-kim.github.io/skos-einfuehrung/#/skos-elemente)
- ## Demo: Publikation mit SkoHub Vocabs (Docker)
  Schritte:
  1. Fork von skohub-docker-vocabs auf GitHub
  2. Kleine Anpassungen der GitHub-Actions-Konfiguration im Repo
  3. GitHub Actions aktivieren
  4. Bau eines eigenen Vokabulars
  5. git push und Publikation
  6. Anlegen von Perma-URIs
- ## 1. Fork von skohub-docker-vocabs auf GitHub
  -> https://github.com/skohub-io/skohub-docker-vocabs
  ![](../assets/fork-skohub-docker-vocabs.png)
- ## 2. Anpassung der GitHub-Actions-Konfiguration
  * Klick im Fork-Repo auf den Ordner .github/workflows
  * Klick auf die Datei `main.yml` und anschließend auf den “Edit”-Button (✏️)
  * Zeile 34: auf EUREN GitHub-Namen ändern, z.B. `git clone https://github.com/acka47/skohub-docker-vocabs.git data/`
- ## 3. GitHub Actions aktivieren
  ![](../assets/github-actions-aktivieren.png)
- ## 4. Eigenes Vokabular schreiben
  * Nutzung [bereits mit SkoHub Vocabs publizierter Vokabulare](https://pad.gwdg.de/s/skohub-booth-at-swib21#Example-Vocabs) als Vorlage/Orientierung
  * Dieser [Turtle Web-Editor](https://felixlohmeier.github.io/turtle-web-editor/) kann zur RDF-Validierung benutzt werden
- ## 5. git push & Publikation
  * mit `git push` bzw. einem direkten commit auf GitHub, wird das Vokabular gebaut und unter `https://{GitHub-username}.github.io/skohub-docker-vocabs/` veröffentlicht
  * Das Bauen und Publizieren kann ein wenig dauern.
- ## 6. Anlegen von Perma-URIs
  Siehe dazu [Anleitung aus dem SkoHub-Workshop bei der SWIB20](https://github.com/skohub-io/swib20-workshop/blob/main/resources/publish-vocab.md#step-6-set-up-redirect-for-persistent-identifiers)
- ## Mehr Anleitungen
  Siehe die Punkte *SKOS* und *SkoHub Vocabs* unter [[Weiterführendes Lernmaterial]]
- ## Beispielvokabulare
  id:: 61e87ac9-a11a-4492-bbfd-66730cfa9a6e
  * Vom Kompetenzzentrum Interoperable Daten (KIM) publiziert:
    * [Hochschulfächersystematik](https://w3id.org/kim/hochschulfaechersystematik/scheme)
    * [Resource Types](https://w3id.org/kim/hcrt/scheme)
    * [Schulfächer](http://w3id.org/kim/schulfaecher/)
  * Kerndatensatz Forschung: [Interdisziplinäre Forschungsfeldklassifikation](https://w3id.org/kdsf-ffk/)
  * Wir lernen online: https://vocabs.openeduhub.de/
  * ZPID, leibniz-psychology.org: http://zpidvokabulare.surge.sh/index.en.html
  * MPIeR Categories of Matters regulated by Police Ordinances: https://w3id.org/rg-mpg-de/polmat/scheme